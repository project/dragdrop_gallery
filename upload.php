<?php

require_once 'ImageResize.php';

$error_message[0] = "Unknown problem with upload.";
$error_message[1] = "Uploaded file too large (load_max_filesize).";
$error_message[2] = "Uploaded file too large (MAX_FILE_SIZE).";
$error_message[3] = "File was only partially uploaded.";
$error_message[4] = "Choose a file to upload.";

$upload_dir = $_SERVER['DOCUMENT_ROOT'].$_GET['filedir'].'/';
$nid = $_GET['nid'];
$num_files = count($_FILES['user_file']['name']);
$success = FALSE;
$sources = $nid . "|";
$upload_array = array();

for ($i=0; $i < $num_files; $i++) {
    $upload_file = $upload_dir . basename($_FILES['user_file']['name'][$i]);
    if (!preg_match("/(gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG)$/",$_FILES['user_file']['name'][$i])) {
        //print "I asked for an image...";
    } else {
        if (is_uploaded_file($_FILES['user_file']['tmp_name'][$i])) {
            if (move_uploaded_file($_FILES['user_file']['tmp_name'][$i], $upload_file)) {
            	$upload_array[] = $upload_file;
						} else {
                //print $error_message[$_FILES['user_file']['error'][$i]];
            }
        } else {
            //print $error_message[$_FILES['user_file']['error'][$i]];
        }
    }
}

for($i = 0; $i < sizeof($upload_array); $i++) {
	$destination = "";
	$uf_array = explode(".", $upload_array[$i]);
	for($j = 0; $j < sizeof($uf_array); $j++) {
		if($j == sizeof($uf_array) - 2) {
			$destination .= $uf_array[$j] . '-100x100.';
		}
		else if($j == sizeof($uf_array) - 1) {
			$destination .= $uf_array[$j];
		}
		else {
			$destination .= $uf_array[$j] . ".";
		}
	}
        			
	ImageResize::image_scale_cropped($upload_array[$i], $destination, 100, 100);
	
	$expl_upload = explode("/", $upload_array[$i]);
	$expl_destination = explode("/", $destination);
							
	$sources .= $expl_upload[count($expl_upload) - 1] . "|" . $expl_destination[count($expl_destination) - 1] . "|";
	$success = FALSE;
}

print($sources);

?>